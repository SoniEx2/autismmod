package io.github.soniex2.autism.asm;

import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

/**
 * @author soniex2
 */
public class PotionEffectTransformer implements IClassTransformer {
	private Map<String, String> maps;
	private boolean isObfuscated;

	private void setupDeobfMaps() {
		maps = new HashMap<String, String>();
		maps.put("mname", "performEffect");
		maps.put("mdesc", "(Lnet/minecraft/entity/EntityLivingBase;)V");
		maps.put("nm_mdesc", "(Lnet/minecraft/potion/PotionEffect;Lnet/minecraft/entity/EntityLivingBase;)Z");
	}

	private void setupObfMaps() {
		maps = new HashMap<String, String>();
		maps.put("mname", "func_76457_b");
		maps.put("mdesc", "(Lnet/minecraft/entity/EntityLivingBase;)V");
		maps.put("nm_mdesc", "(Lnet/minecraft/potion/PotionEffect;Lnet/minecraft/entity/EntityLivingBase;)Z");
	}


	@Override
	public byte[] transform(String name, String transformedName, byte[] basicClass) {
		isObfuscated = !name.equals(transformedName);
		if (transformedName.equals("net.minecraft.potion.PotionEffect"))
			return transformPotionEffect(basicClass);
		return basicClass;
	}

	private byte[] transformPotionEffect(byte[] basicClass) {
		if (isObfuscated) setupObfMaps();
		else setupDeobfMaps();

		System.out.println("Transforming PotionEffect");

		try {
			ClassNode classNode = new ClassNode();
			ClassReader classReader = new ClassReader(basicClass);
			classReader.accept(classNode, 0);

			boolean transformed = false;

			for (MethodNode m : classNode.methods) {
				if (m.name.equals(maps.get("mname")) && m.desc.equals(maps.get("mdesc"))) {
					ListIterator<AbstractInsnNode> it = m.instructions.iterator();
					System.out.println("Transforming PotionEffect.performEffect");
					VarInsnNode instance = new VarInsnNode(Opcodes.ALOAD, 0);
					it.add(instance);
					VarInsnNode entity = new VarInsnNode(Opcodes.ALOAD, 1);
					it.add(entity);
					MethodInsnNode call = new MethodInsnNode(Opcodes.INVOKESTATIC,
						"io/github/soniex2/autism/EventHandler",
						"potion",
						maps.get("nm_mdesc"),
						false);
					it.add(call);
					LabelNode cont = new LabelNode();
					JumpInsnNode test = new JumpInsnNode(Opcodes.IFEQ, cont);
					it.add(test);
					InsnNode ret = new InsnNode(Opcodes.RETURN);
					it.add(ret);
					it.add(cont);
					System.out.println("Done!");
					transformed = true;
				}
			}
			if (!transformed)
				System.out.println("Failed to transform PotionEffect.performEffect");

			ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
			classNode.accept(writer);

			if (transformed)
				System.out.println("Transforming PotionEffect - Success!");
			else
				System.out.println("Transforming PotionEffect - Failed!");
			return writer.toByteArray();
		} catch (Exception e) {
			System.out.println("Transforming PotionEffect - Failed!");
			e.printStackTrace();
		}

		return basicClass;
	}
}
