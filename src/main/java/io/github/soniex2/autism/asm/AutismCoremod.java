package io.github.soniex2.autism.asm;

import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * @author soniex2
 */
@IFMLLoadingPlugin.TransformerExclusions("io.github.soniex2.autism.asm")
@IFMLLoadingPlugin.SortingIndex(Integer.MAX_VALUE)
public class AutismCoremod implements IFMLLoadingPlugin {
	@Override
	public String[] getASMTransformerClass() {
		return new String[] {
			"io.github.soniex2.autism.asm.PotionEffectTransformer"
		};
	}

	@Override
	public String getModContainerClass() {
		return null;
	}

	@Nullable
	@Override
	public String getSetupClass() {
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data) {

	}

	@Override
	public String getAccessTransformerClass() {
		return null;
	}
}
