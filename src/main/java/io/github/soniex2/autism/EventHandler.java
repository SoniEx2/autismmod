package io.github.soniex2.autism;

import baubles.api.BaubleType;
import baubles.api.cap.BaublesCapabilities;
import baubles.api.cap.IBaublesItemHandler;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * @author soniex2
 */
public class EventHandler {
	@SubscribeEvent
	public void fireDamageEvent(LivingAttackEvent event) {
		if (event.getSource() != DamageSource.ON_FIRE) {
			return;
		}
		EntityLivingBase entity = event.getEntityLiving();
		IBaublesItemHandler itemHandler = entity.getCapability(BaublesCapabilities.CAPABILITY_BAUBLES, null);
		if (itemHandler == null) {
			return;
		}
		for (int i : BaubleType.AMULET.getValidSlots()) {
			ItemStack is = itemHandler.getStackInSlot(i);
			if (is.getItem() instanceof ItemChewNecklace) {
				((ItemChewNecklace) is.getItem()).checkFireDamage(event, is);
				return;
			}
		}
	}

	public static boolean potion(PotionEffect potion, EntityLivingBase entity) {
		IBaublesItemHandler itemHandler = entity.getCapability(BaublesCapabilities.CAPABILITY_BAUBLES, null);
		if (itemHandler == null) {
			return false;
		}
		for (int i : BaubleType.AMULET.getValidSlots()) {
			ItemStack is = itemHandler.getStackInSlot(i);
			if (is.getItem() instanceof ItemChewNecklace) {
				return ((ItemChewNecklace) is.getItem()).checkPotion(potion, entity, is);
			}
		}
		return false;
	}
}
