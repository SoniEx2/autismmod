package io.github.soniex2.autism;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;
import org.apache.logging.log4j.Logger;

/**
 * @author soniex2
 */
@Mod(modid = "autism", name = "Autism Acceptance Mod", version = "1.0.1", dependencies = "required-after:baubles")
public class Autism {

	/**
	 * 5-minute chew
	 */
	private Item itemChewNecklace1;
	/**
	 * 10-minute chew
	 */
	private Item itemChewNecklace2;
	/**
	 * 20-minute chew
	 */
	private Item itemChewNecklace3;
	/**
	 * Silicone
	 */
	private Item itemCrafting;

	public static Logger log;

	private void preInitCommon(FMLPreInitializationEvent event) {
		/*if (!Loader.isModLoaded("baubles")) {
			throw new RuntimeException("Please install baubles mod (Forge's dependency model sucks, deal with it.)") {
				@Override
				public synchronized Throwable fillInStackTrace() {
					return this;
				}
			};
		}*/
		log = event.getModLog();
		itemChewNecklace1 = GameRegistry.register(new ItemChewNecklace(300 * 20, "chew1"));
		itemChewNecklace2 = GameRegistry.register(new ItemChewNecklace(600 * 20, "chew2"));
		itemChewNecklace3 = GameRegistry.register(new ItemChewNecklace(1200 * 20, "chew3"));
		itemCrafting = GameRegistry.register(new ItemCrafting());
		MinecraftForge.EVENT_BUS.register(new EventHandler());
	}

	@Mod.EventHandler
	@SideOnly(Side.SERVER)
	public void preInitServer(FMLPreInitializationEvent event) {
		preInitCommon(event);
	}

	@Mod.EventHandler
	@SideOnly(Side.CLIENT)
	public void preInitClient(FMLPreInitializationEvent event) {
		preInitCommon(event);
		ModelLoader.setCustomModelResourceLocation(itemChewNecklace1, 0, new ModelResourceLocation(itemChewNecklace1.getRegistryName(), ""));
		ModelLoader.setCustomModelResourceLocation(itemChewNecklace2, 0, new ModelResourceLocation(itemChewNecklace2.getRegistryName(), ""));
		ModelLoader.setCustomModelResourceLocation(itemChewNecklace3, 0, new ModelResourceLocation(itemChewNecklace3.getRegistryName(), ""));
		ModelLoader.setCustomModelResourceLocation(itemCrafting, 0, new ModelResourceLocation(itemCrafting.getRegistryName(), ""));
	}

	private void initCommon(FMLInitializationEvent event) {
		GameRegistry.addShapelessRecipe(new ItemStack(itemCrafting), new ItemStack(Items.COAL, 1, OreDictionary.WILDCARD_VALUE), new ItemStack(Blocks.SAND, 1, OreDictionary.WILDCARD_VALUE));
		GameRegistry.addShapelessRecipe(new ItemStack(itemChewNecklace1), Items.STRING, itemCrafting);
		GameRegistry.addShapelessRecipe(new ItemStack(itemChewNecklace2), Items.STRING, itemCrafting, itemCrafting);
		GameRegistry.addShapelessRecipe(new ItemStack(itemChewNecklace3), Items.STRING, itemCrafting, itemCrafting, itemCrafting);
	}

	@Mod.EventHandler
	@SideOnly(Side.SERVER)
	public void initServer(FMLInitializationEvent event) {
		initCommon(event);
	}

	@Mod.EventHandler
	@SideOnly(Side.CLIENT)
	public void initClient(FMLInitializationEvent event) {
		initCommon(event);
	}
}
