package io.github.soniex2.autism;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * @author soniex2
 */
public class ItemCrafting extends Item {
	public ItemCrafting() {
		setUnlocalizedName("autism:crafting");
		setRegistryName(new ResourceLocation("autism", "crafting"));
		setCreativeTab(CreativeTabs.MATERIALS);
	}
}
