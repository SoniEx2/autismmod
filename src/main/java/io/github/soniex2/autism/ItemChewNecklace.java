package io.github.soniex2.autism;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author soniex2
 */
public class ItemChewNecklace extends Item implements IBauble {
	private static final Field FIRE = ReflectionHelper.findField(Entity.class, "field_190534_ay", "fire");
	private static final String[] PERFORM_EFFECT_NAMES = {"func_76457_b", "performEffect"};
	private static final Class[] PERFORM_EFFECT_SIG = {EntityLivingBase.class};
	private static final Method PERFORM_EFFECT = ReflectionHelper.findMethod(PotionEffect.class, null, PERFORM_EFFECT_NAMES, PERFORM_EFFECT_SIG);

	public ItemChewNecklace(int durability, String name) {
		maxStackSize = 1;
		setMaxDamage(durability);
		setUnlocalizedName("autism:" + name);
		setRegistryName(new ResourceLocation("autism", name));
		setCreativeTab(CreativeTabs.MISC);
	}

	@Override
	public BaubleType getBaubleType(ItemStack itemStack) {
		return BaubleType.AMULET;
	}

	@Override
	public void onWornTick(ItemStack itemstack, EntityLivingBase player) {
		boolean flag = false;
		if (!player.isImmuneToFire()) {
			try {
				int fire = FIRE.getInt(player);
				if (fire > 0) {
					flag = true;
					if (fire % 20 == 0) {
						player.attackEntityFrom(DamageSource.ON_FIRE, 1.0F);
					}
					FIRE.setInt(player, fire - 1);
				}
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		for (PotionEffect potionEffect : player.getActivePotionEffects()) {
			try {
				if (!PERFORM_EFFECT.equals(ReflectionHelper.findMethod((Class) potionEffect.getClass(), null, PERFORM_EFFECT_NAMES, PERFORM_EFFECT_SIG))) {
					String modname = potionEffect.getClass().getName();
					Autism.log.warn("Why is mod " + modname + " overriding PotionEffect.performEffect?");
					continue;
				}
			} catch (ReflectionHelper.UnableToFindMethodException ignored) {
				continue;
			}
			if (potionEffect.onUpdate(player)) {
				flag = true;
			}
		}
		if (flag) {
			itemstack.damageItem(1, player);
		}
	}

	private static boolean workaround; // forge bug

	public void checkFireDamage(LivingAttackEvent event, ItemStack is) {
		if (event.getEntityLiving() instanceof EntityPlayer) {
			workaround = !workaround;
			if (workaround) return;
		}
		// Do fire damage only half the time
		NBTTagCompound tag = is.getOrCreateSubCompound("chew");
		boolean b = tag.getBoolean("fire");
		event.setCanceled(b);
		tag.setBoolean("fire", !b);
	}

	public boolean checkPotion(PotionEffect potion, EntityLivingBase entity, ItemStack is) {
		NBTTagCompound tag = is.getOrCreateSubCompound("chew");
		NBTTagCompound subtag = tag.getCompoundTag("potion");
		String key = Potion.REGISTRY.getNameForObject(potion.getPotion()).toString().intern();
		boolean b = subtag.getBoolean(key);
		subtag.setBoolean(key, !b);
		tag.setTag("potion", subtag);
		return b;
	}
}
